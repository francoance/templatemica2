import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private usuario: string = "";
  private password: string = "";
  private mensajeError: string = "";
  private mostrarMensajeError: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  login():void {
    //acá haces el login en el backend para saber si el usuario existe

    //después de verificar que el usuario existe en el backend y que la contraseña está bien, deberías
    //moverte a otro componente

    //Si algo sale mal, podés poner un mensajito de error así
    this.mensajeError = "Algo salió mal";
    this.mostrarMensajeError = true;
  }

}
