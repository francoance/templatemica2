import { Component, OnInit } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.css']
})
export class EstadisticasComponent implements OnInit {

  title=""
   chart: Estadistica[]=[]
   view: any[] = [900, 500];
   showXAxis = true;
   showYAxis = true;
   gradient = false;
   showLegend = true;
   showXAxisLabel = true;
   xAxisLabel = '';
   showYAxisLabel = true;
   yAxisLabel = '';
   colorScheme = 'picnic';
  constructor() { }

  ngOnInit() {
    this.obtenerDatos();
  }

  obtenerDatos():void {
    //Acá deberías buscar los datos para reportar desde el backend.
    this.chart = [];
    //Primera 
    let dataMica = [];
    dataMica.push({name: "2016", value: 10});
    dataMica.push({name: "2017", value: 40});
    dataMica.push({name: "2018", value: 70});
    dataMica.push({name: "2019", value: 100});
    let estadisticaMica: Estadistica;
    estadisticaMica = {
      data: dataMica,
      nombre: "Nivel de crack de la Mica",
      xAxisLabel: "Año",
      yAxisLabel: "Nivel"
    }
    this.chart.push(estadisticaMica);

    //Segunda
    let dataFranco = [];
    dataFranco.push({name: "2016", value: 10});
    dataFranco.push({name: "2017", value: 40});
    dataFranco.push({name: "2018", value: 70});
    dataFranco.push({name: "2019", value: 100});
    let estadisticaFranco: Estadistica;
    estadisticaFranco = {
      data: dataFranco,
      nombre: "Nivel de crack del Franco",
      xAxisLabel: "Año",
      yAxisLabel: "Nivel"
    }
    this.chart.push(estadisticaFranco);

  }
}

interface Estadistica {
  data?: Array<{ name:string, value:number}>,
  nombre?:string,
  xAxisLabel?:string,
  yAxisLabel?:string,
}